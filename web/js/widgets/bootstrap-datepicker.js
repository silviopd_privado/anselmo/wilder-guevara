var BootstrapDatepicker = {
    init: function() {        
        $("#m_datepicker_2, #m_datepicker_2_validate").datepicker({
            language: 'es',
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "top left"
        })
    }
};
jQuery(document).ready(function() {
    BootstrapDatepicker.init()
});
