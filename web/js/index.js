jQuery(document).ready(function($) {

showSlides();
});




$('#voluntario').click(function() {
    $("#txttipooperacion").val("agregar");
    $("#myModal").modal("show")
});

$('#voluntario2').click(function() {
    $("#txttipooperacion").val("agregar");
    $("#myModal").modal("show")
});
$('#bl_salud').click(function() {
    swal({
        title: "Salud",
        text: "Wilder Guevara Opcion de Salud",
        icon: "success",
        button: "OK",
    });
});
$('#bl_educacion').click(function() {
    swal({
        title: "Educacion",
        text: "Wilder Guevara Opcion de Educacion",
        icon: "success",
        button: "OK",
    });
});
$('#bl_seg').click(function() {
    swal({
        title: "Seguridad",
        text: "Wilder Guevara Opcion de Seguridad",
        icon: "success",
        button: "OK",
    });
});
$('#bl_cul').click(function() {
    swal({
        title: "Cultura",
        text: "Wilder Guevara Opcion de Cultura",
        icon: "success",
        button: "OK",
    });
});

$("#frmgrabar").submit(function(evento) {
    evento.preventDefault();

    if ($("#txttipooperacion").val() == "agregar") {

        var ruta = DIRECCION_WS + "usuario.agregar.php";
        var nombre = $("#modal_nombres").val()
        var direc = $("#modal_direccion").val()
        var cel = $("#modal_cel").val()
        var correo = $("#modal_correo").val()
        var fecha_nac = $("#m_datepicker_2").val()
        fecha_nac = fecha_nac.split("-").reverse().join("-")
        var c_1 = $("#mdC_semana").val();
        var c_2 = $("#mdC_fds").val();
        var c_3 = $("#mdC_mensual").val();
        var tp_1 = $("#mdtp_volanteo").val();
        var tp_2 = $("#mdtp_conversatorio").val();
        var tp_3 = $("#mdtp_caravana").val();
        var h_1 = $("#mdh_mañana").val();
        var h_2 = $("#mdh_tarde").val();
        var h_3 = $("#mdh_noche").val();
        var pp = 2;


        swal({
            title: '¿Desea Registrar?',
            text: "se agregará un nuevo tipo servicio!",
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'registrar',
            cancelButtonText: 'cancelar',
            imageUrl: "../vista/imagenes/pregunta.png"
        }).then(function() {
            $.post(ruta, {
                nombre: nombre,
                direc: direc,
                correo: correo,
                cel: cel,
                fecha_nac: fecha_nac,
                c_1: c_1,
                c_2: c_2,
                c_3: c_3,
                tp_1: tp_1,
                tp_2: tp_2,
                tp_3: tp_3,
                h_1: h_1,
                h_2: h_2,
                h_3: h_3,
                pp: pp
            }, function() {}).done(function(resultado) {
                var datosJSON = resultado;
                if (datosJSON.estado === 200) {
                    swal({
                        title: 'EXITO!',
                        text: datosJSON.mensaje,
                        type: 'success',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                    $("#myModal").modal("hide")
                } else {
                    swal("Mensaje del sistema", resultado, "warning");
                }
            }).fail(function(error) {
                var datosJSON = $.parseJSON(error.responseText);
                swal("Error", datosJSON.mensaje, "error");
            })
        });

    }

});

$("#mdC_semana").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdC_semana").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdC_semana").val("0");
    }
});
$("#mdC_fds").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdC_fds").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdC_fds").val("0");
    }
});
$("#mdC_mensual").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdC_mensual").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdC_mensual").val("0");
    }
});
$("#mdtp_volanteo").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdtp_volanteo").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdtp_volanteo").val("0");
    }
});
$("#mdtp_conversatorio").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdtp_conversatorio").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdtp_conversatorio").val("0");
    }
});
$("#mdtp_caravana").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdtp_caravana").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdtp_caravana").val("0");
    }
});
$("#mdh_mañana").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdh_mañana").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdh_mañana").val("0");
    }
});
$("#mdh_tarde").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdh_tarde").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdh_tarde").val("0");
    }
});
$("#mdh_noche").on('change', function() {
    if ($(this).is(':checked')) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#mdh_noche").val("1");
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#mdh_noche").val("0");
    }
});

function showSlides() {
    var slideIndex = 0;
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";  
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}    
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " active";
    setTimeout(showSlides, 4000); // Change image every 2 seconds
}